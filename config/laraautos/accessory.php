<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'laraautos',

    /*
     * Package.
     */
    'package'   => 'accessory',

    /*
     * Modules.
     */
    'modules'   => ['accessory', 
'accessory_category'],

    'image'    => [

        'sm' => [
            'width'     => '140',
            'height'    => '140',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'md' => [
            'width'     => '320',
            'height'    => '244',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'lg' => [
            'width'     => '780',
            'height'    => '497',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],
        'xl' => [
            'width'     => '824',
            'height'    => '375',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

    ],

    'accessory'       => [
        'model'             => 'Laraautos\Accessory\Models\Accessory',
        'table'             => 'accessories',
        'presenter'         => \Laraautos\Accessory\Repositories\Presenter\AccessoryItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'name'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id', 'status','category_id', 'name',  'description','overview',  'brand_id',  'model_id',  'price',  'images',  'address',  'neighborhood',  'city',  'state',  'country',  'zip_code'],
        'translate'         => [],

        'upload_folder'     => 'accessory/accessory',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => ['images'],
                               ],
        'casts'             => [
                                  'images'=>'array',
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
        'category_id',
            'name'  => 'like',
            'status', 
            'brand_id',
            'model_id',
            'price'  => [
                'condition' => 'between',
                'default'   => [0, 100000000000000],
            ],
            'created_at'=>'like',
            'updated_at'=>'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "Accessory created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "Accessory completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "Accessory verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "Accessory approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "Accessory published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "Accessory unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "Accessory archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "Accessory deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],

    'accessory_category'       => [
        'model'             => 'Laraautos\Accessory\Models\AccessoryCategory',
        'table'             => 'accessory_categories',
        'presenter'         => \Laraautos\Accessory\Repositories\Presenter\AccessoryCategoryItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => [],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id', 'parent_id', 'name',  'description'],
        'translate'         => [],

        'upload_folder'     => 'accessory/accessory_category',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => [],
                               ],
        'casts'             => [
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
           'name'  => 'like',
            'parent_id'  => 'like',
            'created_at'=> 'like',
            'updated_at'=> 'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "AccessoryCategory created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "AccessoryCategory completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "AccessoryCategory verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "AccessoryCategory approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "AccessoryCategory published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "AccessoryCategory unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "AccessoryCategory archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "AccessoryCategory deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
];
