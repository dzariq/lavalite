<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'laraautos',

    /*
     * Package.
     */
    'package'   => 'car',

    /*
     * Modules.
     */
    'modules'   => ['car', 
'brand', 
'car_model'],

    'image'    => [

        'sm' => [
            'width'     => '59',
            'height'    => '59',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'md' => [
            'width'     => '360',
            'height'    => '244',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'lg' => [
            'width'     => '780',
            'height'    => '497',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],
        'xl' => [
            'width'     => '825',
            'height'    => '375',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

    ],

    'car'       => [
        'model'             => 'Laraautos\Car\Models\Car',
        'table'             => 'cars',
        'presenter'         => \Laraautos\Car\Repositories\Presenter\CarItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'name'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id','name', 'status','brand_id',  'model_id',  'description',  'release_year', 'images', 'price',  'condition',  'mileage',    'body_type',  'door_count',  'exterior_color',  'engine',  'engine_type',  'engine_size',  'horse_power',  'transmission',  'drive_type',  'fuel_type',  'features',  'address',  'neighborhood',  'city',  'state',  'country',  'zip_code','featured','slider','premium','latitude','longitude'],
        'translate'         => [],

        'upload_folder'     => 'car/car',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => ['images'],
                               ],
        'casts'             => [
                    'features'=> 'array',
                    'images'=> 'array',
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
            'brand_id',
            'model_id',
            'name'  => 'like', 
            'status'  => 'like',
            'condition'  => 'like',
            'release_year'  => 'like',
            'exterior_color'  => 'like',
            'body_type'  => 'like',
            'price'      =>    [
                'condition' => 'between',
                'default'   => [0, 100000000000000],
            ],            
            'created_at'=>'like',
            'updated_at',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "Car created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "Car completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "Car verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "Car approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "Car published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "Car unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "Car archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "Car deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
'brand'       => [
        'model'             => 'Laraautos\Car\Models\Brand',
        'table'             => 'brands',
        'presenter'         => \Laraautos\Car\Repositories\Presenter\BrandItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'name'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id', 'name',  'description'],
        'translate'         => [],

        'upload_folder'     => 'car/brand',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => [],
                               ],
        'casts'             => [
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
            'name'  => 'like',
            'created_at'=> 'like',
            'updated_at'=> 'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "Brand created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "Brand completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "Brand verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "Brand approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "Brand published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "Brand unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "Brand archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "Brand deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
'car_model'       => [
        'model'             => 'Laraautos\Car\Models\CarModel',
        'table'             => 'car_models',
        'presenter'         => \Laraautos\Car\Repositories\Presenter\CarModelItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'name'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id', 'brand_id',  'name',  'description'],
        'translate'         => [],

        'upload_folder'     => 'car/car_model',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => [],
                               ],
        'casts'             => [
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
            'brand_id',
            'name'  => 'like',
            'created_at'=> 'like',
            'updated_at'=> 'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "CarModel created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "CarModel completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "CarModel verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "CarModel approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "CarModel published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "CarModel unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "CarModel archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "CarModel deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
];
