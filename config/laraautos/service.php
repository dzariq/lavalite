<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'laraautos',

    /*
     * Package.
     */
    'package'   => 'service',

    /*
     * Modules.
     */
    'modules'   => ['service', 
'service_category'],

    'image'    => [

        'sm' => [
            'width'     => '140',
            'height'    => '140',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'md' => [
            'width'     => '150',
            'height'    => '135',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

        'lg' => [
            'width'     => '780',
            'height'    => '497',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],
        'xl' => [
            'width'     => '825',
            'height'    => '375',
            'action'    => 'fit',
            'watermark' => 'img/logo/default.png',
        ],

    ],

    'service'       => [
        'model'             => 'Laraautos\Service\Models\Service',
        'table'             => 'services',
        'presenter'         => \Laraautos\Service\Repositories\Presenter\ServiceItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'title'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id','status', 'category_id',  'title',  'description',  'phone',  'email',  'website',  'skype', 'address',  'neighborhood',  'city',  'state',  'country',  'zip_code','latitude','longitude'],
        'translate'         => [],

        'upload_folder'     => 'service/service',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => ['images'],
                               ],
        'casts'             => [
                                    'images'=>'array',
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
            'category_id'  => '=',
            'title'  => 'like',
            'phone'  => 'like',
            'email'  => 'like',
            'city'  => 'like',
            'state'  => 'like',
            'status', 
            'country'  => 'like',
            'website'=> 'like',
            'created_at'=> 'like',
            'updated_at'=> 'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "Service created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "Service completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "Service verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "Service approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "Service published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "Service unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "Service archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "Service deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
'service_category'       => [
        'model'             => 'Laraautos\Service\Models\ServiceCategory',
        'table'             => 'service_categories',
        'presenter'         => \Laraautos\Service\Repositories\Presenter\ServiceCategoryItemPresenter::class,
        'hidden'            => [],
        'visible'           => [],
        'guarded'           => ['*'],
        'slugs'             => ['slug' => 'name'],
        'dates'             => ['deleted_at'],
        'appends'           => [],
        'fillable'          => ['user_id', 'parent_id',  'name',  'description'],
        'translate'         => [],

        'upload_folder'     => 'service/service_category',
        'uploads'           => [
                                    'single'    => [],
                                    'multiple'  => [],
                               ],
        'casts'             => [
                               ],
        'revision'          => [],
        'perPage'           => '20',
        'search'        => [
            'name'  => 'like',
            'parent_id'  => 'like',
            'created_at'=>'like',
            'updated_at'=>'like',
        ],
        /*
        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "ServiceCategory created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "ServiceCategory completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "ServiceCategory verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "ServiceCategory approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "ServiceCategory published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "ServiceCategory unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "ServiceCategory archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "ServiceCategory deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],
        */
    ],
];
