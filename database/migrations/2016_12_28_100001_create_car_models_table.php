<?php

use Illuminate\Database\Migrations\Migration;

class CreateCarModelsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: car_models
         */
        Schema::create('car_models', function ($table) {
            $table->increments('id');
            $table->integer('brand_id')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('slug', 200)->nullable();
            $table->enum('status', ['draft', 'complete', 'verify', 'approve', 'publish', 'unpublish', 'archive'])->default('draft')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('user_type',50)->nullable();
            $table->string('upload_folder', 100)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('car_models');
    }
}
