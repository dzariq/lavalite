<?php

use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: cars
         */
        Schema::create('cars', function ($table) {
            $table->increments('id');
            $table->integer('brand_id')->nullable();
            $table->integer('model_id')->nullable();
            $table->text('description')->nullable();
            $table->integer('release_year')->nullable();
            $table->decimal('price', 50)->nullable();
            $table->enum('condition', ['New'=>'New','Used'=>'Used'])->nullable();
            $table->float('mileage')->nullable();
            $table->longText('images')->nullable();
            $table->enum('body_type', ['Convertible','Limousine / Sedan','Off-road vehicle','Pickup','Truck','Hatchback','Sports car / Coupe','Van / Minibus','Other'])->nullable();
            $table->enum('door_count', ['2/3','4/5','6/7'])->nullable();
            $table->enum('exterior_color', ['Beige','Black','Blue','Brown','Charcoal','Gold','Green','Grey','Orange','Pink','Purple','Red','Silver','Tan','Violet','White','Yellow'])->nullable();
            $table->enum('engine', ['3 Cylinders','4 Cylinders','5 Cylinders','6 Cylinders','8 Cylinders','10 Cylinders','12 Cylinders'])->nullable();
            $table->enum('engine_type', ['V','R','H'])->nullable();
            $table->string('engine_size', 100)->nullable();
            $table->string('horse_power', 100)->nullable();
            $table->enum('transmission', ['Manual','Automatic','Semi-automatic'])->nullable();
            $table->enum('drive_type', ['Front wheel drive','Rear wheel drive','All wheel drive'])->nullable();
            $table->enum('fuel_type', ['Gasoline','Gas','Diesel','Other'])->nullable();
            $table->longText('features')->nullable();
            $table->text('address')->nullable();
            $table->string('neighborhood', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('slug', 200)->nullable();
            $table->enum('status', ['draft', 'complete', 'verify', 'approve', 'publish', 'unpublish', 'archive'])->default('draft')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('user_type',50)->nullable();
            $table->string('upload_folder', 100)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('cars');
    }
}
