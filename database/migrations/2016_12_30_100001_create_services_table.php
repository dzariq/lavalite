<?php

use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: services
         */
        Schema::create('services', function ($table) {
            $table->increments('id');
            $table->integer('category_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
            $table->integer('phone')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->string('skype', 100)->nullable();
            $table->longText('images')->nullable();
            $table->longText('logo')->nullable();
            $table->text('address')->nullable();
            $table->string('neighborhood', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('slug', 200)->nullable();
            $table->enum('status', ['draft', 'complete', 'verify', 'approve', 'publish', 'unpublish', 'archive'])->default('draft')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('user_type',50)->nullable();
            $table->string('upload_folder', 100)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('services');
    }
}
