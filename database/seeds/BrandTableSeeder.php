<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('brands')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'brand.brand.view',
                'name'      => 'View Brand',
            ],
            [
                'slug'      => 'brand.brand.create',
                'name'      => 'Create Brand',
            ],
            [
                'slug'      => 'brand.brand.edit',
                'name'      => 'Update Brand',
            ],
            [
                'slug'      => 'brand.brand.delete',
                'name'      => 'Delete Brand',
            ],
            /*
            [
                'slug'      => 'brand.brand.verify',
                'name'      => 'Verify Brand',
            ],
            [
                'slug'      => 'brand.brand.approve',
                'name'      => 'Approve Brand',
            ],
            [
                'slug'      => 'brand.brand.publish',
                'name'      => 'Publish Brand',
            ],
            [
                'slug'      => 'brand.brand.unpublish',
                'name'      => 'Unpublish Brand',
            ],
            [
                'slug'      => 'brand.brand.cancel',
                'name'      => 'Cancel Brand',
            ],
            [
                'slug'      => 'brand.brand.archive',
                'name'      => 'Archive Brand',
            ],
            */
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/brand/brand',
                'name'        => 'Brand',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/brand/brand',
                'name'        => 'Brand',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'brand',
                'name'        => 'Brand',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'key'      => 'brand.brand.key',
                'name'     => 'Some name',
                'value'    => 'Some value',
                'type'     => 'Default',
            ],
            */
        ]);
    }
}
