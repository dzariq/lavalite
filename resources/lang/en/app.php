<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for All Module
    |--------------------------------------------------------------------------
    |
     */

    'name'          => 'Laraautos',
    'name.html'     => '<b>Lara</b>Autos',
    'name.short'    => '<b>L</b>l',
    'admin.panel'   => 'Admin Panel',
    'dashboard'     => 'Dashboard',
    'all.rights'    => "<p>© 2016 Laraautos. All rights reserved Powered by <a href=''>Lavalite</a></p>",
    'version'       => '<b>Version</b> Develop',

    'add'           => 'Add',
    'approve'       => 'Approve',
    'archive'       => 'Archived',
    'unarchive'     => 'Unarchived',
    'back'          => 'Back',
    'cancel'        => 'Cancel',
    'close'         => 'Close',
    'copy'          => 'Copy',
    'complete'      => 'Completed',
    'create'        => 'Create',
    'dashboard'     => 'Dashboard',
    'delete'        => 'Delete',
    'draft'         => 'Draft',
    'details'       => 'Details',
    'edit'          => 'Edit',
    'go'            => 'Go',
    'help'          => 'Help',
    'home'          => 'Home',
    'logout'        => 'Logout',
    'logs'          => 'Logs',
    'manage'        => 'Manage',
    'more'          => 'More',
    'new'           => 'New',
    'no'            => 'No',
    'opt'           => 'Options',
    'option'        => 'Option',
    'options'       => 'Options',
    'order'         => 'Order',
    'password'      => 'Password',
    'profile'       => 'Profile',
    'publish'       => 'Published',
    'request'       => 'Request',
    'reset'         => 'Reset',
    'save'          => 'Save',
    'search'        => 'Search',
    'settings'      => 'Settings',
    'show'          => 'Show',
    'status'        => 'Status',
    'update'        => 'Update',
    'update_profile'=> 'Update Profile',
    'unpublish'     => 'Unpublished',
    'view'          => 'View',
    'verify'        => 'Verifyed',
    'yes'           => 'Yes',

    'flash.success' => 'Success',
    'flash.error'   => 'Error',
    'flash.warning' => 'Warning',
    'flash.info'    => 'Info',

];
