<section class="about-section bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section_title">{!!$page->title!!}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-block description">
                            {!!$page->content!!}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {!!Block::get()!!}